# HashFlare-Dark-Theme
Modifies CSS on HashFlare.io to create a dark theme.

## Easy Install
Download and install the Stylish extension:  
Chrome: https://chrome.google.com/webstore/detail/stylish-custom-themes-for/fjnbnpbmkenffdnngjfgmeleoegfcffe  
Firefox: https://addons.mozilla.org/en-US/firefox/addon/stylish/  
Opera: https://addons.opera.com/nb/extensions/details/stylish/  

Install the style: https://userstyles.org/styles/154209/hashflare-dark-theme 

## Preview of Current Code
![CoinMarketCap](https://jaany.xyz/i/hashflare.png)
